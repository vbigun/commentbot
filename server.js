const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});



app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/race', /*passport.authenticate('jwt'),*/ function (req, res) {
  res.sendFile(path.join(__dirname, 'race.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.email === userFromReq.email);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
    // io.connect();
  } else {
    res.status(401).json({ auth: false });
  }


});


let activeUsers = [];
let gameTimer = 120;

io.on('connection', socket => {

  

  socket.on('login', payload => {
    const { token } = payload;
    const email = jwt.decode(token).email;

    // socket.on('text', payload => {
    //   console.log(payload.text.length);
    //   if ()
    // });
   
    if (activeUsers.map(x => x.user.email).includes(email)) {
      // console.log('user already playing');
      socket.emit('logout', {})
      socket.disconnect();
    } else {
      // console.log('new user');
      const json = {
        socket: socket,
        user: jwt.decode(token),
        token: token,
        score: 0
      }
      activeUsers.push(json);
      socket.broadcast.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
      socket.emit('newUsers', { users: activeUsers.map(x => x.user.email) });

      const startText = 'На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійстово Вам буду я, Ескейп Ентерович і я радий вас вітати зі словами Доброго Вам дня панове!';
      io.sockets.emit('startBot', { text: startText });
        
    }

    if(activeUsers.length > 1) {

      let playersText;
      let player = activeUsers.map(x => x.user.email);

      if(activeUsers.length == 2) {
        playersText = `А тим часом список гонщиків: ${player[0]} на ferrari під номером 1. Під номером 2  у нас ${player[1]} на своїй Lambo`;
      } else if (activeUsers.length == 3) {
        playersText = `А тим часом список гонщиків: ${player[0]} на ferrari під номером 1, gід номером 2  у нас ${player[1]} на своїй Lambo та під номером 3 ${player[2]} на BMW`;
      }

      socket.on('calculateSymbols', payload => {
          console.log(payload.currentLength);
      });

      io.sockets.emit('startRace', { text: playersText });

      let interval = setInterval(() => { 
        io.sockets.emit('game', { time: gameTimer });

        gameTimer--;

        /////
        let bubbleSort = (activeUsers) => {
          // console.log(activeUsers);
          let len = activeUsers.length;
          for (let i = 0; i < len; i++) {
              for (let j = 0; j < len; j++) {
                  if (activeUsers[j + 1] && activeUsers[j].score < activeUsers[j + 1].score) {
                      let tmp = activeUsers[j];
                      activeUsers[j] = activeUsers[j + 1];
                      activeUsers[j + 1] = tmp;
                  }
              }
          }
          return activeUsers;
        };

        const sortedUsers = bubbleSort(activeUsers);

        let currentSorted = sortedUsers.map(x => { return { email: x.user.email, score: x.score } });
        let differenceFirstSecond = currentSorted[0].score - currentSorted[1].score;
       
        let currentTime = 120 - gameTimer;
        if(gameTimer % 30 == 0 && gameTimer !== 0) {
          let thirtySecondsText;
          if(currentSorted.length == 2) {
            thirtySecondsText = `Першим іде ${currentSorted[0].email}. Його наздоганяє ${currentSorted[1].email}. Ріниця між ними ${differenceFirstSecond} слів(слова). До кінця гонки залишилось: ${currentTime} секунд`; 
          } else {
            let differenceSecondThird = currentSorted[1].score - currentSorted[2].score;
            thirtySecondsText = `Першим іде ${currentSorted[0].email}. Його наздоганяє ${currentSorted[1].email}. Ріниця між ними ${differenceFirstSecond} слів(слова). За ними йде ${currentSorted[2].email}. Різниця між 2 та 3 учасником ${differenceSecondThird}. Ось така трійка лідерів. До кінця гонки залишилось: ${currentTime} секунд`;
          }
          io.sockets.emit('currentComment', { text: thirtySecondsText});
        }

        // принимаем со фронта
        // socket.on('text', payload => {
        //   console.log(payload, 'строка 132');
        // });
        //

        if(gameTimer === -1) {
          clearInterval(interval);


          let finalResult = sortedUsers.map(x => { return { email: x.user.email, score: x.score } });
          console.log(finalResult[0].email);

          let finalText;
          if (finalResult.length == 2) {
            finalText = `Перше місце занимає ${finalResult[0].email} набираючи ${finalResult[0].score} очок, а друге ${finalResult[1].email} набираючи ${finalResult[1].score} очок. Усім дякую за увагу) З вами був Ескейп Ентерович. До нових зустрічей!`;
          } else if (finalResult.length == 3) {
            finalText = `Перше місце займає ${finalResult[0].email} набираючи ${finalResult[0].score} очок, а друге ${finalResult[1].email} набравши ${finalResult[1].score} очок і третя строчка за ${finalResult[2].email} який набрав ${finalResult[2].score} очкок. Усім дякую за увагу) З вами був Ескейп Ентерович. До нових зустрічей!`;
          }

          io.sockets.emit('finalText', { text: finalText });
          
          io.sockets.emit('endGame', { results: sortedUsers.map(x => { return { email: x.user.email, score: x.score } }) });
          gameTimer = 120;
        }
      }, 1000);

     
    }

  });

  socket.on('update', payload => {
    console.log(payload.score);
    
    
    
    const x = activeUsers.filter(a => a.token === payload.token)[0];
    const userToUpdate = x.user;
    const token = x.token;
    activeUsers.filter(u => u.token === payload.token)[0].score = payload.score;
    if (payload.score == 30) {
      let thirtyWordsText = `Пользователю ${userToUpdate.email} осталось совсем не много!`;
      io.sockets.emit('thirtyWordsText', {text:thirtyWordsText });
    }
    // console.log(userToUpdate.email);
    socket.emit('updateScore', { email: userToUpdate.email, score: payload.score });
    socket.broadcast.emit('updateScore', { email: userToUpdate.email, score: payload.score });
  });

  socket.on('disconnect', payload => {
    // console.log('disconnecting...')
    activeUsers = activeUsers.filter(json => json.socket != socket);
    // console.log(activeUsers);
    socket.broadcast.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
    socket.emit('newUsers', { users: activeUsers.map(x => x.user.email) });
  });

  
});